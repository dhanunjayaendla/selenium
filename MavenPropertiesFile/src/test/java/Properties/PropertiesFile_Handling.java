package Properties;

import java.io.FileInputStream;

import java.io.IOException;
import java.util.Properties;

public class PropertiesFile_Handling {

	public static void main(String[] args) throws IOException {
		FileInputStream fis=new FileInputStream("C:\\Users\\edhan\\OneDrive\\Desktop\\seleniumeclipse\\MavenPropertiesFile\\src\\Config\\Variables.Properties");
		Properties p=new Properties();
		p.load(fis);
		System.out.println(p.getProperty("username"));
		System.out.println(p.getProperty("password"));
		System.out.println(p.getProperty("mail"));
	}

}
