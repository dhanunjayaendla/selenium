package ExceptionHandle;

public class Basic_ {

	public static void main(String[] args) {
		//to avoid the abnormal termination of the program Exception Handling is used
		
		/*
		 * keywords use in exception handling are 
		 *1.try
		 *2.catch
		 *3.throw
		 *4.throws
		 *5.finally
		 *
		 *1.try :it consists of a block of code in which we are expecting the  exeptions
		 *
		 * 2.catch():It handles the Exception raised in the try block
		 * 
		 * Exception types:
		 * 1.checked Exception--compile time exceptions
		 * 2.unchecked Exception--run time exceptions
		 * 3.errors
		 * 
		 * Run time exceptions are :
		 * 1.ArithmeticException
		 * 2.NumberFormatException
		 * 3.NullPointerException
		 * 4.ArrayIndexOutOfBoundsException
		 * 
		 * all the above exception are available in Throwable Class
		 * (java.lang.package)
		 * 
		 * 1.ArithmeticException:-->int a=5/0;/by zero
		 * 2.NumberFormatException--->String str="Hello";,int num=Integer.parseInt(str);
		 * 3.NullPointerException--->String str=null;syso(str.leangth());
		 * 4.ArrayIndexOutOfBoundsException--->int a[]=new int[5];,a[6]=10;
		 * 
		 */


	}

}
