package ExceptionHandle;

public class ArthimaticExceptionHandling {
	//this is unchecked exception---i.e run time exception
	//to handle this exception we can use try catch or throws methods

	public static void main(String[] args) {
		try {
			int a = 10;
			int b = 0;
			int c = a / b;
			System.out.println(c);

		} catch (ArithmeticException e) {
			System.out.println("The Value Of b Sholud Not be Zero");
			System.out.println(e);		//to print the type of exception
			
			System.out.println(e.getMessage());//to get the the message
		}
	}

}
