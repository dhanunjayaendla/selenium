package ExceptionHandle;
//unchecked Exception

public class SampleClass2 {
	//handling the exception at the time of method calling  using try catch blocks only as it is a unchecked exception there is no need to use throws key word
	
	//different solutions will be executed at the time of calling in this way for the same method as follows


	public static void main(String[] args) {
		tc1();
		tc2();

	}

	public static void tc1() {
		try {
			method1(100, 0);

		} catch (Exception e) {
			System.out.println("Exception Generated in tc1. There is No alternative Solution");
		}

	}
	
	public static void tc2() {
		try {
			method1(100, 0);

		} catch (Exception e) {
			System.out.println("Exception Generated  in tc2. And method1 will be called again here");
			method1(100, 5);
		}

		
	}

	public static void method1(int A, int B) {
		int C = A / B;
		System.out.println(C);

	}

}
