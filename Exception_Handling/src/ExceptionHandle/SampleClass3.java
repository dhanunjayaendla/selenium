package ExceptionHandle;
//Checked Exception Handling Only One way handling

public class SampleClass3 {

	public static void main(String[] args) {
		tc1();
		tc2();
		

	}

	public static void tc1() {
		waitMethod(2);

	}

	public static void tc2() {
		waitMethod(3);

	}

	public static void waitMethod(int timeInSeconds) {
		try {
			Thread.sleep(1000 * timeInSeconds);
			System.out.println("try block is executed");
		} catch (InterruptedException e) {
			System.out.println("Interrupted.");
		}

	}

}
