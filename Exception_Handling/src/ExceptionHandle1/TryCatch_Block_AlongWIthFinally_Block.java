package ExceptionHandle1;

public class TryCatch_Block_AlongWIthFinally_Block {

	public static void main(String[] args) {
		m1(100, 0);//finally block will be execute irrespective of catch block but it will be used along with try catch blocks only
		m1(100, 20);
		
	}
	public static void m1(int A,int B) {
		try {
			int C=A/B;
			System.out.println(C);
			
			
		} catch (ArithmeticException e) {
			System.out.println("B Can Not Be Zero");
			
		}
		finally {
			System.out.println("This is Final Block");
		}
		
	}

}
