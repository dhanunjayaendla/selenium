package ExceptionHandle1;
//Conclusion is that for unchecked Exception There is no need To use throws Keyword To forward The exception to The 
//another method 

//but For Checked We Must Use The Throws KeyWord To Handle it in Another Methods.but In the Same Handling Only try catch Blocks are sufficient there is no need Of using The Throws Keyword
public class TryCatchFor_UnChecked_WithOut_Throws {

	public static void main(String[] args) {
		tc1();
		tc2();
	}
	public static void tc1() {
		try {
			m1(100, 0);
		} catch (ArithmeticException e) {
			System.out.println("This is TestCase1 Catch Block .There is No alternative Solution");
		}

	}

	public static void tc2() {
		try {
			m1(100, 0);
		} catch (ArithmeticException e) {
			System.out.println(
					"This Is TestCase2 Catch Block .Here again m1 method 1 can be called to solve the Exception");
			m1(100, 20);
		}

	}

	public static void m1(int A, int B)  {
		int C = A / B;
		System.out.println(C);

	}


}
