package ExceptionHandle1;

public class TryCatchFor_UnChecked_WIth_Throws {

	public static void main(String[] args) {
		tc1();
		tc2();

	}

	public static void tc1() {
		try {
			m1(100, 0);
		} catch (ArithmeticException e) {
			System.out.println("This is TestCase1 Catch Block .There is No alternative Solution");
		}

	}

	public static void tc2() {
		try {
			m1(100, 0);
		} catch (ArithmeticException e) {
			System.out.println(
					"This Is TestCase2 Catch Block .Here again m1 method 1 can be called to solve the Exception");
			m1(100, 20);
		}

	}

	public static void m1(int A, int B) throws ArithmeticException {
		int C = A / B;
		System.out.println(C);

	}
}
