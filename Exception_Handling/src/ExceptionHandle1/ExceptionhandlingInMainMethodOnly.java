package ExceptionHandle1;

import java.util.Scanner;

public class ExceptionhandlingInMainMethodOnly {

	public static void main(String[] args) {
		try {
			Scanner sc=new Scanner(System.in);
			System.out.println("Enter The Value Of A : ");
			int A=sc.nextInt();
			System.out.println("Enter the Value Of B : ");
			int B=sc.nextInt();
			int C=A/B;
			System.out.println("The Division Of A by B is : "+C);
			sc.close();

		} catch (ArithmeticException e) {
			System.out.println("The Value Of B Can Not Be Zero");
		}
		
	}

}
