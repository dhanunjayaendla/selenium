package TestNGParallelExecution;

import org.testng.annotations.Test;

public class ClassA {
	@Test(priority = 0)
	public void mul() {
		int a=10;
		int b=2;
		int c=a*b;
		System.out.println("the multiplication of a and b is : "+c);
	}
	@Test(priority = 1)
	public void div() {
		int a=10;
		int b=2;
		int c=a/b;
		System.out.println("the division of a and b is : "+c);
	}

}
