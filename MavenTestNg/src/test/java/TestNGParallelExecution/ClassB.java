package TestNGParallelExecution;

import org.testng.annotations.Test;

public class ClassB {
	@Test(priority = 0)
	public void add() {
		int a=10;
		int b=2;
		int c=a+b;
		System.out.println("the addition of a and b is : "+c);
	}
	@Test(priority = 1)
	public void sub() {
		int a=10;
		int b=2;
		int c=a-b;
		System.out.println("the substraction of a and b is : "+c);
	}

}
