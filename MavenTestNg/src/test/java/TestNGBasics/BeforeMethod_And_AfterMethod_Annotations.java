package TestNGBasics;

import org.testng.annotations.AfterMethod;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

//before method will be executed before every test case
//-->that means it will executed number of test cases times

//after method will be executed after every test case
//-->that means it will executed number of test cases times

public class BeforeMethod_And_AfterMethod_Annotations {
	@BeforeMethod
	public void login() {
		System.out.println("this is Login method");
		
	}
	@AfterMethod
	public void logout() {
		System.out.println("this is logout method");
		
	}
	@Test
	public void TC1() {
		System.out.println("this is test case1");
		
	}
	@Test
	public void TC2() {
		System.out.println("this is test case2");
		
	}

}
