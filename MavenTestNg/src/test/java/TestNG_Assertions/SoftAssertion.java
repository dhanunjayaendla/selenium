package TestNG_Assertions;

import java.util.Scanner;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;
//soft Assertion -->it will not Throw Exception Immediately it will store the Exception in The Buffer
//the Remaining code after This code Will be Continued 
//all the Exceptions Will be called Where we Want to call by SoftAssert.AssertAll();

//it is Non Static So We Need To Create Object To Use This Method
//Using The Created Object We can Use The Soft Assert Class Methods 

public class SoftAssertion {
	@Test
	public void add() {
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter The Value Of A : ");
		int a=sc.nextInt();
		System.out.println("Enter The Value Of B : ");
		int b=sc.nextInt();
		int c=a+b;
		System.out.println("Expected Value of C is :");
		int exp=sc.nextInt();
		
		SoftAssert softassert=new SoftAssert();
		softassert.assertEquals(c, exp,"the sum of A and B is mismatched");
		sc.close();
		System.out.println("This Program Will Be executed Even Though Exception Occures As It is A Soft Assert Class");
		softassert.assertAll();
		//It is used to call The all Exceptions Occurred In this Class
		//if we Dont Use this method The test Will Show It As PASSED even Though It is failed Actually
		
	}
}
