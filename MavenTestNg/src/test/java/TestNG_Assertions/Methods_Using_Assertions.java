package TestNG_Assertions;

import java.util.Scanner;

import org.testng.Assert;
import org.testng.annotations.Test;

public class Methods_Using_Assertions {
	@Test
	public void add() {
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter The Value Of A : ");
		int a=sc.nextInt();
		System.out.println("Enter The Value Of B : ");
		int b=sc.nextInt();
		int c=a+b;
		System.out.println("Expected Value of C is :");
		int exp=sc.nextInt();
		//HardAssert/verify--->it will throw immediate exception the remaining code after this 
		//will be terminated automatically
		Assert.assertEquals(c, exp,"the sum of A and B is mismatched");
		sc.close();
		System.out.println("This Program Will Not Be executed If Exception Occures As It is A hard Assert Class");
		
	}

}
