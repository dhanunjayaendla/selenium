package TestNGParameterFile;

import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

public class Parameters1 {
	@Parameters({"a","b"})
	@Test(priority = 0)
	public void add(int a,int b) {
		int c=a+b;
		System.out.println("the addition of a and b is : "+c);
		
	}
	@Parameters({"a","b"})
	@Test(priority = 1,invocationCount = 3)
	public void sub(int a,int b) {
		int c=a-b;
		System.out.println("the substraction of a and b is : "+c);
		
	}
	@Parameters({"a","b"})
	@Test(priority = 2,enabled = false)
	public void mul(int a,int b) {
		int c=a*b;
		System.out.println("the multipliaction of a and b is : "+c);
		
	}
	@Parameters({"a","b"})
	@Test(priority = 2)
	public void div(int a,int b) {
		int c=a/b;
		System.out.println("the division of a and b is : "+c);
		
	}

}
