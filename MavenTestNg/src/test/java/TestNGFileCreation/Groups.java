package TestNGFileCreation;

import org.testng.annotations.Test;

public class Groups {
	@Test(groups = "buyer")
	public void groupUse1() {
		System.out.println("i am in the group Use1 of Groups");

	}
	@Test(groups = "user")
	public void groupUse2() {
		System.out.println("i am in the group Use2 of Groups");

	}
	@Test(groups = "buyer")
	public void groupUse3() {
		System.out.println("i am in the group Use3 of Groups");

	}
	@Test(groups = "seller")
	public void groupUse4() {
		System.out.println("i am in the group Use4 of Groups");

	}
	@Test(groups = "seller")
	public void groupUse5() {
		System.out.println("i am in the group Use5 of Groups");
	}





}
