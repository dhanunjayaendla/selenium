package TestNGFileCreation;
//xml file structure should be as follows

import org.testng.annotations.Test;

/*
 * 1.Suite---->parallel=tests
 * (Properties)
 * (Groups)--->run--->include(or exclude)
 * these two are optional 
 * 2.Test
 * 3.Classes
 * 4.Class
 * 5.Methods
 * 
 * xml file is used to execute test cases according to our requirement
 */

public class TestNgXML {
	@Test
	public void testMethod1() {
		System.out.println("i am in the test method 1 of TestngXML");

	}
	@Test
	public void testMethod2() {
		System.out.println("i am in the test method 2 of TestngXML");

	}
	@Test
	public void testMethod3() {
		System.out.println("i am in the test method 3 of TestngXML");

	}
	@Test
	public void testMethod4() {
		System.out.println("i am in the test method 4 of TestngXML");

	}
	@Test
	public void testMethod5() {
		System.out.println("i am in the test method 5 of TestngXML");
	}

}
