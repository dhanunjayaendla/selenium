package TestNGFileCreation;

import org.testng.annotations.Test;

public class Groups1 {
	@Test(groups = "user")
	public void group1Method1() {
		System.out.println("i am in the group1 method 1 of Groups1");

	}
	@Test(groups = {"admin","seller"})
	public void group1Method2() {
		System.out.println("i am in the group1 method 2 of Groups1");

	}
	@Test(groups = {"admin","buyer"})//multiple group ids
	public void group1Method3() {
		System.out.println("i am in the group1 method 3 of Groups1");

	}
	@Test(groups = "user")
	public void group1Method4() {
		System.out.println("i am in the group1 method 4 of Groups1");

	}
	@Test(groups = "user")
	public void group1Method5() {
		System.out.println("i am in the group1 method 5 of Groups1");
	}



}
