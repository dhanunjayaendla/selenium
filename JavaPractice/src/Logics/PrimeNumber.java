package Logics;

import java.util.Scanner;

public class PrimeNumber {

	public static void main(String[] args) {
		Scanner sc=new Scanner(System.in);
		System.out.println("Please Enter the Number ");
		int num=sc.nextInt();
		int count=0;
		for (int i = 1; i <= num; i++) {
			if (num%i==0) {
				count++;
				
			}
			
		}
		if (count==2) {
			System.out.println("The Given Number is A Prime Number");
			
		}else {
			System.out.println("The Given Number is Not A Prime Number");
		}
		sc.close();
	}

}
