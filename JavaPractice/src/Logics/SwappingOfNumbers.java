package Logics;

public class SwappingOfNumbers {

	public static void main(String[] args) {
		int a=10;
		int b=20;
		System.out.println("before swapping :");
		System.out.println("the valu of a : "+a);
		System.out.println("the value of b : "+b);
		b=a+b;
		a=b-a;
		b=b-a;
		System.out.println("after swapping :");
		System.out.println("the value of a : "+a);
		System.out.println("the value of b : "+b);
		
	}

}
