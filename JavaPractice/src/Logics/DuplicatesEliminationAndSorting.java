package Logics;

import java.util.TreeSet;

public class DuplicatesEliminationAndSorting {

	public static void main(String[] args) {
		int[] numrs = { 10, 12, 19, 19, 18, 16, 23, 25, 54, 35, 23, 45, 54 };
		TreeSet<Integer> ts=new TreeSet<>();
		for (int i : numrs) {
			ts.add(i);
			
			
		}
		System.out.println("sorted set eliminates duplicates and ascending order is followed as follows :");
		System.out.println(ts);

	}

}
