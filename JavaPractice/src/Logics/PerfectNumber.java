package Logics;

import java.util.Scanner;

public class PerfectNumber {

	public static void main(String[] args) {
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter The Number To Check Weather it is A Perfect Number Or Not");
		int num=sc.nextInt();
		int sumOfFactors=0;
		for (int i = 1; i < num; i++) {
			if (num%i==0) {
				sumOfFactors=sumOfFactors+i;
				
			}
			
		}
		if (sumOfFactors==num) {
			System.out.println("The Given Number i.e  "+num+" is A Perfect Number");
			
		}else {
			System.out.println("The Given Number i.e  "+num+" is Not A Perfect Number");
			
		}
		sc.close();
	}

}
