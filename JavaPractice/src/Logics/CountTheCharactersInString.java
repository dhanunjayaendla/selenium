package Logics;

import java.util.TreeMap;

public class CountTheCharactersInString {

	public static void main(String[] args) {
		String str="Dhanunjaya";
		TreeMap<Character, Integer> tm=new TreeMap<>();
		for (int i = 0; i < str.length(); i++) {
			char myChar=str.charAt(i);
			if (tm.containsKey(myChar)) {
				int val=tm.get(myChar);
				val++;
				tm.put(myChar, val);
				
			}else {
				tm.put(myChar, 1);
			}
			
		}
		System.out.println(tm.entrySet());
	}

}
