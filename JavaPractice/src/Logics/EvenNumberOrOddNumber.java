package Logics;

import java.util.Scanner;

public class EvenNumberOrOddNumber {

	public static void main(String[] args) {
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter The Number To Check Weather it is a Even Or Odd");
		int num=sc.nextInt();
		if (num%2==0) {
			System.out.println("the given number i.e "+num+" is An Even Number");
			
		}else {
			System.out.println("the given number i.e "+num+" is An Odd Number");
		}
		sc.close();
	}

}
