package Logics;

import java.util.Scanner;

public class NumberPolyndrome {

	public static void main(String[] args) {
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter the Number To Check Weather It is A Polyndrome or Not");
		int num=sc.nextInt();
		int actnum=num;
		int revnum=0;
		while (num!=0) {
			int rem=num%10;
			revnum=revnum*10+rem;
			num=num/10;
			
		}
		if (actnum==revnum) {
			System.out.println("the Given Number Is A Polyndrome Number");
			
		}else {
			System.out.println("The Given Number is Not A Polyndrome");
		}
		sc.close();
	}

}
