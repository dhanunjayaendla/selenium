package Logics;

import java.util.Scanner;

public class NumberReversing {
	public static void main(String[] args) {
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter the Number To Reverse");
		int num=sc.nextInt();
		int actnum=num;
		int revnum=0;
		while (num!=0) {
			int rem=num%10;
			revnum=revnum*10+rem;
			num=num/10;
			
			
		}
		System.out.println("the reversed number of "+actnum+" is "+revnum);
		sc.close();

	}
	
}
