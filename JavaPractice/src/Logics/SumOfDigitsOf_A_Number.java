package Logics;

public class SumOfDigitsOf_A_Number {

	public static void main(String[] args) {
		int num=12345;
		int real=num;
		int sumOfDigits=0;
		while (num!=0) {
			int rem=num%10;
			sumOfDigits=sumOfDigits+rem;
			num=num/10;
			
			
		}
		System.out.println("The sum of the digits of the number "+real+" is : "+sumOfDigits);
	}

}
