package Logics;

import java.util.ArrayList;
import java.util.TreeSet;

public class ArrayLargestNumber {

	public static void main(String[] args) {
		int[] numrs = { 10, 12, 19, 19, 18, 16, 23, 25, 54, 35, 23, 45, 54 };
		TreeSet<Integer> ts=new TreeSet<>();
		for (int i : numrs) {
			ts.add(i);
			
		}
		System.out.println(ts);
		
		ArrayList<Integer>al=new ArrayList<>(ts);
		//to print the largest number in the given array
		System.out.println(al.get(al.size()-1));
		//to print the second largest number in the given array
		System.out.println(al.get(al.size()-2));
		
		//to print the third largest number in the given array
		System.out.println(al.get(al.size()-3));

	}

}
