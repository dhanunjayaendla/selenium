package Logics;

public class ToFindThePrimeDigitOnly {

	public static void main(String[] args) {
		int num = 2347894;

		while (num != 0) {
			int count = 0;
			int rem = num % 10;
			for (int i = 1; i <= rem; i++) {
				if (rem % i == 0) {
					count++;
				}
			}
			if (count == 2) {
				System.out.println(rem);
			}
			num = num / 10;

		}
	}

}
