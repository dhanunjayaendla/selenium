package XLHandling;

import java.io.FileInputStream;

import java.io.IOException;

import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class XLSheet {

	public static void main(String[] args) throws IOException {
		FileInputStream fis=new FileInputStream("C:\\Users\\edhan\\OneDrive\\Desktop\\MYPROJECT1.xlsx");
		XSSFWorkbook wb=new XSSFWorkbook(fis);
		XSSFSheet sheet=wb.getSheet("Sheet1");
		XSSFRow row=sheet.getRow(1);
		
		//to get the name of the person
		XSSFCell name=row.getCell(1);
		System.out.println("The Name of The Employee is : "+name.toString());//to get the name of the person
		
		//to get the salary of the person
		XSSFCell salary=row.getCell(2);
		System.out.println("The Salary Of The "+name.toString()+" is : "+salary.toString());
		
		////to get the tax of the persons salary
		XSSFCell tax=row.getCell(4);
		System.out.println("The Tax Amount Of "+name.toString()+ " On His Salary of "+salary.toString()+" is "+tax.getNumericCellValue());
		wb.close();
		
		
	}

}
