package Basics;

import java.util.Scanner;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import io.github.bonigarcia.wdm.WebDriverManager;

public class CrossBrowsing_2 {
	static WebDriver driver;

	public static void main(String[] args) {
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter The Browser Name :");
		String browserName="firefox";
		sc.close();
		
		launchbrowser(browserName, "https://www.facebook.com/");
		launchbrowser("chrome", "https://www.google.co.in/");

	}

	public static void launchbrowser(String browserName, String url) {
		switch (browserName) {
		case "chrome":
			WebDriverManager.chromedriver().setup();
			driver = new ChromeDriver();
			break;
		case "firefox":
			WebDriverManager.firefoxdriver().setup();
			driver = new FirefoxDriver();
			break;
		case "edge":
			WebDriverManager.edgedriver().setup();
			driver = new EdgeDriver();
			break;

		}
		driver.get(url);
//		driver.navigate().to("https://www.facebook.com/");
		// any one of the above can be used to navigate to particular url
	}

}
